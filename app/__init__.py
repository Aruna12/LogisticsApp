from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, template_folder='../templates', static_folder='../static')
app.config.from_object('config')
app.secret_key = 'JinjaMaster'

db = SQLAlchemy(app)

from app import views, models


