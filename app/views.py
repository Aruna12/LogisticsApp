from flask import render_template, request, flash, redirect, url_for, session
from app import app, db
from app.models import User, Logistic, Requirements
import uuid
import json

temp_id = 0
@app.route('/testx', methods=['GET'])
def testx():
    session['loggedIn'] = False
    return json.dumps({'msg':'logged-out'})

@app.route('/register/', methods=['GET'])
def getRegister():
    print("Inside Get Register")
    return render_template("auth/register.html")

@app.route('/postRegister/', methods=['POST'])
def postRegister():
    if request.method == 'POST':
        userId = str(uuid.uuid1())
        name = request.form.get('name', '')
        password = request.form.get('password', '')
        email = request.form.get('email', '')
        userdetails = User(userId, name, password, email)
        db.session.add(userdetails)
        db.session.commit()
        flash("You were successfully registered into the system")

    return redirect(url_for('StartingPage'))


@app.route('/addItem', methods=['GET'])
def addProduct():
    user = session['username']
    return render_template('admin/additem.html', currentuser=user)

@app.route('/addItem', methods=['POST'])
def addItem():
    if session['username'] == "admin":
        id = str(uuid.uuid1())

        name = request.form.get('name', '')
        qty = request.form.get('quantity', '')
        print(qty)
        details = Logistic(id,name,qty)
        db.session.add(details)
        db.session.commit()
        flash('Successfully added Item')
        return redirect(url_for('addProduct'))
    else:
        return "Invalid Credentials"

@app.route('/updateProduct', methods=['GET'])
def updateProduct():
    user = session['username']
    product = Logistic.query.all()
    return render_template('admin/update.html', currentuser=user, products=product)

@app.route('/allItems', methods=["GET"])
def allItems():
    user = session['username']
    products = Logistic.query.all()
    return render_template('admin/allitem.html', currentuser=user, products=products)


@app.route('/updateProduct', methods=["POST"])
def postProductUpdate():
    product = request.form.get('product', '')
    qty = request.form.get('quantity', '')
    qty = int(qty)
    prod = Logistic.query.filter_by(name=product).first()

    prod.quantity = prod.quantity + qty
    db.session.commit()
    flash("Successfully Updated")
    return redirect(url_for('updateProduct'))

@app.route('/placeRequirement', methods=['GET'])
def getRequirement():
    user = session['username']
    product = Logistic.query.all()
    return render_template('events/putrequirement.html', currentuser=user, products=product)

@app.route('/placeRequirement', methods=["POST"])
def postRequirement():
    id = str(uuid.uuid1())
    item = request.form.get('product', '')
    qty = request.form.get('quantity', '')
    user = session['username']
    req = Requirements(id,user,item,qty,quantity_approved=0,coreapp=-1)

    db.session.add(req)
    db.session.commit()
    flash("Successfully Updated")
    return redirect(url_for('getRequirement'))

@app.route('/checkRequirement', methods=['GET'])
def checkRequirement():
    user = session['username']
    req = Requirements.query.filter_by(username = user).all()
    return render_template('events/checkorders.html', currentuser = user, requirements = req)

@app.route('/approveRequirements', methods=["GET"])
def getApproveRequirement():
    user = session['username']
    req = Requirements.query.filter_by(coreapp=-1).all()
    return render_template('admin/approverequirements.html', currentuser=user, requirements=req)

@app.route('/editApprovedRequiremtens', methods=["GET"])
def getEditApprovedRequirements():
    user = session['username']
    req = Requirements.query.filter_by(coreapp=1).all()
    return render_template('admin/editrequirement.html', currentuser = user, requirements = req)

@app.route('/postApprovedRequiremrents', methods=["POST"])
def postEditApprovedRequirements():
    id = request.form.get('id', '')
    item = request.form.get('item', '')
    qty_new = request.form.get('quantity', '')
    qty_new = int(qty_new)
    item_name = Logistic.query.filter_by(name=item).first()

    if qty_new > item_name.quantity:
        flash("Not Enough Materials Left")
        return redirect(url_for('getEditApprovedRequirements'))

    req = Requirements.query.filter_by(id=id).first()
    if qty_new > req.quantity_approved:
        diff = qty_new - req.quantity_approved
        req.quantity_approved = qty_new
        db.session.commit()
        item_name.quantity = item_name.quantity - diff
        db.session.commit()
    else:
        diff = req.quantity_approved - qty_new
        req.quantity_approved = qty_new
        db.session.commit()
        item_name.quantity = item_name.quantity + diff
        db.session.commit()
    flash("Updated Approved Requirements")
    return redirect(url_for('getEditApprovedRequirements'))

@app.route('/editItem', methods=["GET"])
def preGetEditItem():
    user = session['username']
    id = request.args.get('id', '')
    product = Logistic.query.filter_by(id=id).first()
    return render_template('admin/edititem.html', currentuser=user, product=product)

@app.route('/postEditItem', methods=["POST"])
def postEditItem():
    currentuser = session['username']
    id = request.form.get('id', '')
    name = request.form.get('name', '')
    qty = request.form.get('quantity', '')
    prod = Logistic.query.filter_by(id=id).first()
    prod.name = name
    prod.id = id
    prod.quantity = qty
    db.session.commit()
    flash("Successfully Edited")
    return  redirect(url_for('allItems'))


@app.route('/approveReq', methods=["POST"])
def postApproveRequirement():
    id = request.form.get('id', '')
    qty_app = request.form.get('approved_quantity', '')
    qty_app = int(qty_app)
    item = request.form.get('item', '')
    item_name = Logistic.query.filter_by(name=item).first()
    if item_name.quantity == 0 or qty_app > item_name.quantity:
        flash("Not Enough Materials Left")
        return redirect(url_for('getApproveRequirement'))
    print(qty_app)
    item_name.quantity = item_name.quantity - qty_app
    db.session.commit()
    req = Requirements.query.filter_by(id=id).first()
    req.quantity_approved = req.quantity_approved + qty_app
    print(req.quantity_approved)
    db.session.commit()
    req.coreapp = 1
    db.session.commit()
    flash("Updated")
    return redirect(url_for('getApproveRequirement'))

@app.route('/allRequiremets', methods=["GET"])
def allRequirements():
    user = session['username']
    req = Requirements.query.all()
    return render_template('admin/allRequirements.html', currentuser=user, requirements=req)


@app.route('/deleteRequirement', methods=["GET"])
def deleteRequirement():
    user = session['username']
    deleteId = request.args.get('id', '')
    print(deleteId)
    req1 = Requirements.query.all()
    for req in req1:
        print(req.id)
        print(req.item)
    req = Requirements.query.filter_by(id=deleteId).first()
    print(req)
    db.session.delete(req)
    db.session.commit()

    flash("Successfully Deleted the Order")
    return redirect(url_for('checkRequirement'))

@app.route('/postlogin', methods=['POST'])
def postLogin():

    username = request.form.get('username', '')
    password = request.form.get('password', '')

    users = User.query.all()

    for user in users:
        if user.name == username:
            if password == user.password:
                session['loggedIn'] = True
                session['userId'] = user.id
                session['username'] = username
                return redirect(url_for('StartingPage'))
            else:
                flash("Wrong Password")
                return redirect(url_for('getLogin'))
    flash("Wrong User Name")
    return redirect(url_for('getLogin'))


@app.route('/login', methods=['GET'])
def getLogin():
    return render_template('auth/login.html')

@app.route('/logout', methods=['GET'])
def postlogout():
    session['loggedIn'] = False
    return redirect(url_for('StartingPage'))

@app.route('/', methods=['GET'])
def StartingPage():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            user = session['username']
            return render_template('main.html', currentuser=user)

    return render_template('auth/login.html')


